from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .models import *
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
# from .apps import IndexConfig
from .forms import *



# Create your tests here.
class ByeCovidTest(TestCase):
    # Authentication
    def test_login_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_forgot_password_page__url_is_exist(self):
        response = Client().get('/accounts/password/reset/')
        self.assertEqual(response.status_code, 200)

    def test_done_password_page_is_exist(self):
        response = Client().get('/accounts/password/reset/done/')
        self.assertEqual(response.status_code, 200)

    def test_reset_done_page_url_is_exist(self):
        response = Client().get('/accounts/password/reset/key/done/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_is_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginUser)

    def test_login_page_is_using_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_page_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_page_is_using_signup_function(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)

    def test_signup_page_is_using_correct_html(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    #landing page
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPage.html')

    #html
    def test_transaction_page_is_using_correct_html(self):
        response = Client().get('/transaction/')
        self.assertTemplateUsed(response, 'transactionPage.html')

    def test_handsanitizer_page_is_using_correct_html(self):
        response = Client().get('/handsanitizer/')
        self.assertTemplateUsed(response, 'handsanitizer.html')

    def test_masker_page_is_using_correct_html(self):
        response = Client().get('/masker/')
        self.assertTemplateUsed(response, 'masker.html')

    def test_apd_page_is_using_correct_html(self):
        response = Client().get('/apd/')
        self.assertTemplateUsed(response, 'apd.html')

    def test_forgot_password_page_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/')
        self.assertTemplateUsed(response, 'account/password_reset.html')

    def test_done_password_page_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/done/')
        self.assertTemplateUsed(response, 'account/password_reset_done.html')

    def test_reset_done_page_url_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/key/done/')
        self.assertTemplateUsed(response, 'account/password_reset_from_key_done.html')

    #model
    def test_jenis_model(self):
        Jenis.objects.create(nama='APD')
        count = Jenis.objects.all().count()
        self.assertEqual(count, 1)

    def test_product_model(self):
        Jenis.objects.create(nama='APD')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, jenis=Jenis.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        count = Product.objects.all().count()
        self.assertEqual(count, 1)

    #views
    def test_product_page_is_not_none(self):
        self.assertIsNotNone(indexProduct)

    #url
    def test_product_page_url_is_exist(self):
        Jenis.objects.create(nama='APD')
        Product.objects.create(nama='test', harga=1000000, deskripsi='a test',
                               stok=25, jenis=Jenis.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        response = Client().get('/product/1')
        self.assertEqual(response.status_code, 200)

    #form
    # def test_form_post(self):
    #     signUp_form = SignUpForm(data={'email':'abcd@gmail.com', 'full_name': 'abcd'})
    #     self.assertFalse(signUp_form.is_valid())
    #     self.assertEqual(signUp_form.cleaned_data['email'],"abcd@gmail.com")
    #     self.assertEqual(signUp_form.cleaned_data['full_name'],"abcd")

    #apps
    # def test_apps(self):
    #     self.assertEqual(IndexConfig.name, 'homepage')
    #     self.assertEqual(apps.get_app_config('homepage').name, 'homepage')