from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class TransactionCreateForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = '__all__'
        widgets = {
            'orderId': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'customer': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'produk': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'transDate': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'noHP': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'alamat': forms.Textarea(attrs={
                'class': 'form-control'
            }),
        }

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=30, required=False, help_text='Optional.')
    full_name = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = User
        fields = ('username', 'full_name','email', 'password1', 'password2')