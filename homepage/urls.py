from django.conf.urls import url
from django.contrib import admin
from django.urls import path, register_converter
from django.conf.urls.static import static
from .views import *

app_name = 'homepage'

urlpatterns = [
    path('', indexMain, name='indexMain'),
    path('transaction/', indexTransaction, name='indexTransaction'),
    # path('login/', loginFunc, name='login'),
    # path('logout/', logoutFunc, name='logout'),
    # path('signup/', signup, name='signup'),
    path('signup/', signUp, name="signup"),
    path('login/', loginUser, name="loginUser"),
    path('logout/', logoutUser, name="logoutUser"),
    path('apd/', apd, name="apd"),
    path('handsanitizer/', handsanitizer, name="handsanitizer"),
    path('masker/', masker, name="masker"),
    path('product/<str:pk>', indexProduct, name='indexProduct'),

    # path('category/', category, name="category"),

    
    
]