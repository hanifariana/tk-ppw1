from django.db import models
from django.core.validators import *
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.contrib.auth.models import User

class Jenis(models.Model):
    pilihanJenis = (
        ('APD', 'APD'),
        ('Masker', 'Masker'),
        ('Hand Sanitizer', 'Hand Sanitizer')
    )
    nama = models.CharField(max_length=120, choices=pilihanJenis)

    def __str__(self):
        return self.nama

class Product(models.Model):

    nama = models.CharField(max_length=120)
    harga = models.IntegerField()
    deskripsi = models.TextField()
    jenis = models.ForeignKey(Jenis, null=True, on_delete=models.SET_NULL)
    stok = models.IntegerField()
    gambar = models.TextField()

    def __str__(self):
        return self.nama

class Transaction(models.Model):

    orderId = models.CharField(max_length=120)
    customer = models.CharField(max_length=120)
    transDate = models.DateTimeField(auto_now=True)
    produk = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL)
    noHP = models.CharField(max_length=12)
    alamat = models.TextField()

    def __str__(self):
        return self.customer
