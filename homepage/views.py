from django.shortcuts import render, redirect
from .models import *
from .forms import *
from datetime import datetime, date
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.db.models import Q
from django.template.loader import render_to_string
from django.http import JsonResponse

# Create your views here.
def indexMain(request):
    return render(request, 'landingPage.html')

def indexTransaction(request):
    transactions = Transaction.objects.all()

    context = {
        'transactions': transactions,
    }
    return render(request, 'transactionPage.html', context)

def signUp(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            messages.success(request, "Successfully created a user with username {}".format(username))
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})

def loginUser(request): 
    if request.method == 'POST': 
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'Logged in as {}'.format(username))
            return redirect('/')
        else:
            messages.error(request, 'Username or password is incorrect')
    return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    messages.success(request, 'Successfully logged out!')
    return redirect('/')

def apd(request):
    return render(request, 'apd.html', {'apd_list': Product.objects.filter(jenis__nama__contains='APD')})

def masker(request):
    return render(request, 'masker.html', {'masker_list': Product.objects.filter(jenis__nama__contains='Masker')})

def handsanitizer(request):
    return render(request, 'handsanitizer.html', {'handsanitizer_list': Product.objects.filter(jenis__nama__contains='Hand Sanitizer')})

def indexProduct(request, pk):
    selectedProduct = Product.objects.get(id=pk)
    orderIdTmp = selectedProduct.nama
    print(orderIdTmp)
    initial_data = {
        'orderId': orderIdTmp,
        'transDate': datetime.now(),
        'produk': selectedProduct,
    }
    form = TransactionCreateForm(request.POST or None, initial=initial_data)
    print(initial_data)
    # reviews = Review.objects.filter(produk=selectedProduct)
    if form.is_valid():
        print('save berhasil')
        form.save()
        selectedProduct.stok -= 1
        selectedProduct.save()
        messages.success(request, 'Produk berhasil dipesan')
    else:
        print(form.errors)
        print("gagal")

    context = {
        'selectedProduct': selectedProduct,
        'form': form,
        # 'reviews': reviews,
    }
    return render(request, 'productPage.html', context)

# def category(request):
#     return render(request, 'category.html', {'classy_list': Product.objects.filter(style__nama__contains='Classy')})